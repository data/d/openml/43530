# OpenML dataset: Domestic-Football-results-from-1888-to-2019

https://www.openml.org/d/43530

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
I have gathered this dataset over the course of 8 years and put a lot of effort in
it (see soccerverse.com). If you use the data for any kind of project, please drop me a line
or ping me on twitter.
Content
The dataset contains results of 1,078,214 football games in 207 top-tier domestic leagues and 
20 international tournaments (UEFA EuroLeague/ChampionsLeague,etc.) from 1888-2019



variable
description




home
home team name (not necessarily unique)


away
away team name (not necessarily unique)


date
date of match


gh
goals for home team (including extra time and penalties)


ga
goals for away team (including extra time and penalties)


full_time
"F"=game ended in 90', "E"=extra time, "P"=penalty shoot-out


competition
country name of league or name of international competition


home_ident
unique identifier of home team


away_ident
unique identifier of away team


home_country
country of home team


away_country
country of away team


home_code
country code of home team


away_code
country code of away team


home_continent
continent of home team


away_continent
continent of away team


continent
continent of competition


level
"national"= domestic league, "international"= international cup

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43530) of an [OpenML dataset](https://www.openml.org/d/43530). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43530/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43530/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43530/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

